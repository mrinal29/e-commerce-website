import React from 'react';
import './App.css';

import HomePage from './Pages/Homepage/Homepage.component';

function App() {
  return (
    <div>
      <HomePage />
    </div>
  );
}

export default App;
