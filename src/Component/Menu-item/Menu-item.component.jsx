import React from 'react';

import './Menu-item.styles.scss';

const MenuItem = (props) => {

  console.log("props",props.data);
  const {title, imageUrl,size}= props.data;
  return(
    <div
      className={`${size} menu-item`}
      style={{
        backgroundImage: `url(${imageUrl})`
      }}
    >
    <div className='content'>
      <h1 className='title'>{title.toUpperCase()}</h1>
      <span className='subtitle'>SHOP NOW</span>
    </div>
    </div>
  );
  }

export default MenuItem;
