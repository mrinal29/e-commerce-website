import React from 'react';

import Directory from '../../Component/Directory/Directory.component';

import './Homepage.styles.scss';

const HomePage = () => (
  <div className='homepage'>
    <Directory />
  </div>
);

export default HomePage;
